# picluster
https://github.com/k3s-io/k3s

```
echo 'noquiet nosplash cgroup_enable=memory swapaccount=1' >> sudo /boot/cmdline.txt
reboot
curl -sfL https://get.k3s.io | sudo sh -
TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token) && echo $TOKEN
MYSERVER=$(hostname)


curl -sfL https://get.k3s.io | K3S_URL=https://${MYSERVER}:6443 K3S_TOKEN=${TOKEN} sudo sh -

```

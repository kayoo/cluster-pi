#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="launch.sh"
TITLE="PI CLUSTER - BIGHERO"
MENU="Choose one of the following options:"

OPTIONS=(1 "test: ping"
         2 "Installation"
         3 "Updating"
         4 "When in doubt...Reboot!" 
         5 "STOP, ~hammertime")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)  echo "-- You chose Option 1: TEST PING"
            ansible -i inventory.yml all -m ping
            ;;
        2)  echo "-- You chose Option 2: INSTALL"
            ansible-playbook -i inventory.yml playbooks/install.yml
            ;;
        3)  echo "-- You chose Option 3: UPGRADE"
            ansible-playbook -i inventory.yml -b playbooks/upgrade.yml
            ;;
        4)  echo "-- You chose Option 4: REBOOT"
            ansible-playbook -i inventory.yml playbooks/reboot.yml
            ;;
        5)  echo "-- You chose Option 4: HALT"
            ansible-playbook -i inventory.yml playbooks/shutdown.yml
            ;;
esac



